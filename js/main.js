jQuery(function ($) {
  $("#countdown").countdown("2019/11/25", function(event) {
    $("#countdown-day").text(
      event.strftime('%D')
    );

    $("#countdown-hour").text(
      event.strftime('%H')
    );

    $("#countdown-minute").text(
      event.strftime('%M')
    );

    $("#countdown-second").text(
      event.strftime('%S')
    );
  });

  
  $("#accordion").accordion({ heightStyle: "content", collapsible: true });
  $("#accordion1").accordion({ heightStyle: "content", collapsible: true });
  
  $(".owl-carousel").owlCarousel({
    responsiveClass: true,
    responsiveRefreshRate: true,
    margin: 30,
    dots: false,
    nav: true,
    responsive : {
        0 : {
            items: 1
        },
        480 : {
          items: 2
        },
        768 : {
            items: 3
        },
        960 : {
            items: 4
        },
        1200 : {
            items: 6
        },
        1920 : {
            items: 6
        }
    }
     
  }); 
  
  $("a.scrollto").click(function() {
    var elementClick = $(this).attr("href")
    var destination = $(elementClick).offset().top;
    jQuery("html:not(:animated),body:not(:animated)").animate({
      scrollTop: destination
    }, 800);
    return false;
  });
  
  $("a#hidden-content").fancybox({
		'hideOnContentClick': true
	});
  
});




